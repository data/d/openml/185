# OpenML dataset: baseball

https://www.openml.org/d/185

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Database of baseball players and play statistics, including 'Games_played', 'At_bats', 'Runs', 'Hits', 'Doubles', 'Triples', 'Home_runs', 'RBIs', 'Walks', 'Strikeouts', 'Batting_average', 'On_base_pct', 'Slugging_pct' and 'Fielding_ave' 

Notes:  
* Quotes, Single-Quotes and Backslashes were removed, Blanks replaced with Underscores
* Player is an identifier that should be ignored when modelling the data

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/185) of an [OpenML dataset](https://www.openml.org/d/185). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/185/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/185/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/185/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

